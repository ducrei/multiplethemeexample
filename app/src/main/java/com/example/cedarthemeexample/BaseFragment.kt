package com.example.cedarthemeexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment : Fragment() {

    enum class THEME {
        SPRING,
        SUMMER,
        FALL,
        WINTER,
        CO_OP,
        GARAGE
    }

    private fun initializeTheme(theme: THEME): Int {
        return when (theme) {
            THEME.SPRING -> R.style.spring
            THEME.SUMMER -> R.style.summer
            THEME.FALL -> R.style.fall
            THEME.WINTER -> R.style.winter
            THEME.CO_OP -> R.style.co_op
            THEME.GARAGE -> R.style.garage_theme
        }
    }

    protected open fun getThemeType(): THEME {
        return THEME.SPRING
    }

    abstract fun initializeLayout(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val contextThemeWrapper = ContextThemeWrapper(activity, initializeTheme(getThemeType()))

        val localInflater = inflater.cloneInContext(contextThemeWrapper)

        return localInflater.inflate(initializeLayout(), container, false)
    }
}