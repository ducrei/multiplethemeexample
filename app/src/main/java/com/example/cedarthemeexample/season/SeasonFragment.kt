package com.example.cedarthemeexample.season

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.cedarthemeexample.BaseFragment
import com.example.cedarthemeexample.MyAdapter
import com.example.cedarthemeexample.R
import kotlinx.android.synthetic.main.season_fragment.*

class SeasonFragment: BaseFragment() {

    var theme: THEME = THEME.SPRING

    companion object {
        fun newInstance(theme: THEME): SeasonFragment {
            val instance = SeasonFragment()
            instance.theme = theme
            return instance
        }
    }

    override fun initializeLayout(): Int {
        return R.layout.season_fragment
    }

    override fun getThemeType(): THEME {
        return theme
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (theme) {
            THEME.SPRING -> lblSeason.text = getString(R.string.spring)
            THEME.SUMMER -> lblSeason.text = getString(R.string.summer)
            THEME.FALL -> lblSeason.text = getString(R.string.fall)
            THEME.WINTER -> lblSeason.text = getString(R.string.winter)
            THEME.CO_OP -> lblSeason.text = getString(R.string.co_op_theme)
            THEME.GARAGE -> lblSeason.text = getString(R.string.garage)
        }

        btnSpring.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.container, SeasonFragment.newInstance(THEME.SPRING)).addToBackStack("").commit()
        }

        btnSummer.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.container, SeasonFragment.newInstance(THEME.SUMMER)).addToBackStack("").commit()
        }

        btnFall.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.container, SeasonFragment.newInstance(THEME.FALL)).addToBackStack("").commit()
        }

        btnWinter.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.container, SeasonFragment.newInstance(THEME.WINTER)).addToBackStack("").commit()
        }

        btnGarage.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.container, SeasonFragment.newInstance(THEME.GARAGE)).addToBackStack("").commit()
        }

        btnCoop.setOnClickListener {
            fragmentManager!!.beginTransaction().replace(R.id.container, SeasonFragment.newInstance(THEME.CO_OP)).addToBackStack("").commit()
        }

        initRecyclerView()
    }

    private fun initRecyclerView() {

        val viewManager = LinearLayoutManager(context)
        val data = ArrayList<String>()
        for (i in 1..100) {
            data.add("Data $i")
        }
        val viewAdapter = MyAdapter(data)

        my_recycler_view.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

        }
    }
}