package com.example.cedarthemeexample

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.cedarthemeexample.season.SeasonFragment
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().replace(R.id.container, SeasonFragment.newInstance(BaseFragment.THEME.SPRING)).commit()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
